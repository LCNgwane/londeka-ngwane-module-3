import 'package:flutter/material.dart';
// import 'package:login_app/main.dart';
import 'package:login_app/Screens/dashboard.dart';

class LogIn extends StatelessWidget {
  const LogIn({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Login"),
      ),
      body: ElevatedButton(
        onPressed: () => {
          Navigator.push(
            context, MaterialPageRoute(builder: (context) => const Dashboard(),)
            ),
        },
        child: const Text("Sign up"),
        ),
    );
  }
  // @override
  // Widget build(BuildContext context) {
  //   return Scaffold(
  //     body: SafeArea(
  //       child: ListView(
  //         padding: const EdgeInsets.symmetric(horizontal: 18.0),
  //         children: <Widget>[
  //           Column(
  //             children: <Widget>[
  //               const SizedBox(height: 120,),
  //               Image.asset('lib/assets/login.png'),
  //               const SizedBox(height: 30,),
  //               const Text('LogIn', style: TextStyle(fontSize: 25,color: Colors.green),)
  //             ],
  //             ),
  //             const SizedBox(height: 60.0,),
  //             const TextField(
  //               decoration: InputDecoration(
  //                 labelText: "Email",
  //                 labelStyle: TextStyle(fontSize: 20),
  //                 filled: true,
  //               ),
  //             ),
  //             const SizedBox(height: 20.0,),
  //             const TextField(
  //               obscureText: true,
  //               decoration: InputDecoration(
  //                 labelText: "Password",
  //                 labelStyle: TextStyle(fontSize: 20),
  //                 filled: true,
  //               ),
  //             ),
  //             const SizedBox(height: 20,),
  //             Column(
  //               children: <Widget>[
  //                 ButtonTheme(
  //                   height: 50,
  //                   disabledColor: Colors.blueAccent,
  //                   child:  const RaisedButton(
  //                     disabledElevation: 4.0,
  //                     onPressed: null,
  //                     child: Text('Submit',style: TextStyle(fontSize: 20,color: Colors.white),),
  //                   ),
  //                 ),
  //                 const SizedBox(height: 20,),
  //                 const Text('Dont have an account? Sign up Here'),
  //               ],
  //             ),
  //         ],
  //       ),
  //       ),
  //   );
}