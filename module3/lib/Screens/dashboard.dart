import 'package:flutter/material.dart';
import 'package:login_app/widgets/dash_board.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          decoration: const BoxDecoration(
        image: DecorationImage(
            image: AssetImage("lib/assets/dashboard2.jpg"), fit: BoxFit.cover),
      )),
      drawer: const NavDrawer(),
      appBar: AppBar(
        title: const Text('Menu'),
      ),
    );
  }
}